FROM docker.io/debian:buster-slim AS base

ENV DEBIAN_FRONTEND=noninteractive \
    PXC_BUILD=/usr/src/percona-xtradb-cluster/pxc-build \
    PXC_BRANCH=8.0 \
    XTRA_BACKUPS_REPO=https://github.com/percona/percona-xtrabackup/archive/refs/tags \
    XTRA_BACKUPS2_VERSION=2.4.22 \
    XTRA_BACKUPS8_VERSION=8.0.23-16

RUN set -x \
    && apt-get update \
    && echo "# Installing Percona Cluster Build Dependencies" \
    && apt-get -y install git scons gcc g++ openssl check cmake bison \
	libboost-all-dev libasio-dev libaio-dev libncurses5-dev \
	libreadline-dev libpam-dev socat libcurl4-openssl-dev \
	libldap2-dev pkg-config patchelf \
    && echo "# Installing Percona XtraBackup Build Dependencies" \
    && apt-get -y install xxd libev-dev build-essential flex automake autoconf \
	bzr libtool libaio-dev default-mysql-client libncurses-dev zlib1g-dev \
	libgcrypt20-dev python-sphinx wget \
    && cd /usr/src \
    && echo "# Cloning Git Repository" \
    && git clone https://github.com/percona/percona-xtradb-cluster.git

RUN set -x \
    && echo "# Initializing Git Submodules" \
    && cd /usr/src/percona-xtradb-cluster \
    && git checkout $PXC_BRANCH \
    && git submodule init wsrep-lib \
    && git submodule update wsrep-lib \
    && git submodule init percona-xtradb-cluster-galera \
    && git submodule update percona-xtradb-cluster-galera \
    && git submodule init extra/coredumper \
    && git submodule update extra/coredumper \
    && cd wsrep-lib \
    && git submodule init wsrep-API/v26 \
    && git submodule update wsrep-API/v26 \
    && cd ../percona-xtradb-cluster-galera \
    && git submodule init wsrep/src \
    && git submodule update wsrep/src \
    && git submodule init \
    && git submodule update

RUN set -x \
    && echo "# Building Percona XtraBackup $XTRA_BACKUPS2_VERSION" \
    && mkdir -p $PXC_BUILD/pxc_extra/boost \
    && wget $XTRA_BACKUPS_REPO/percona-xtrabackup-$XTRA_BACKUPS2_VERSION.tar.gz \
	-O $PXC_BUILD/percona-xtrabackup-$XTRA_BACKUPS2_VERSION.tar.gz \
    && tar -C $PXC_BUILD/pxc_extra \
	-xzf $PXC_BUILD/percona-xtrabackup-$XTRA_BACKUPS2_VERSION.tar.gz \
    && cd $PXC_BUILD/pxc_extra/percona-xtrabackup-percona-xtrabackup-$XTRA_BACKUPS2_VERSION \
    && cmake -DBUILD_CONFIG=xtrabackup_release \
	-DDOWNLOAD_BOOST=1 \
	-DWITH_BOOST=$PXC_BUILD/pxc_extra/boost \
    && make -j4 \
    && mkdir ../percona-xtrabackup-$XTRA_BACKUPS2_VERSION \
    && make DESTDIR=../percona-xtrabackup-$XTRA_BACKUPS2_VERSION install \
    && cd ../percona-xtrabackup-$XTRA_BACKUPS2_VERSION \
    && for f in bin lib xtrabackup-test; do \
	cp -rp ./usr/local/xtrabackup/$f $f; \
    done \
    && ls

RUN set -x \
    && echo "# Building Percona XtraBackup $XTRA_BACKUPS8_VERSION" \
    && wget $XTRA_BACKUPS_REPO/percona-xtrabackup-$XTRA_BACKUPS8_VERSION.tar.gz \
	-O $PXC_BUILD/percona-xtrabackup-$XTRA_BACKUPS8_VERSION.tar.gz \
    && tar -C $PXC_BUILD/pxc_extra \
	-xzf $PXC_BUILD/percona-xtrabackup-$XTRA_BACKUPS8_VERSION.tar.gz \
    && cd $PXC_BUILD/pxc_extra/percona-xtrabackup-percona-xtrabackup-$XTRA_BACKUPS8_VERSION \
    && cmake -DBUILD_CONFIG=xtrabackup_release \
	-DDOWNLOAD_BOOST=1 \
	-DFORCE_INSOURCE_BUILD=1 \
	-DWITH_BOOST=$PXC_BUILD/pxc_extra/boost \
    && make -j4 \
    && mkdir ../percona-xtrabackup-$XTRA_BACKUPS8_VERSION \
    && make DESTDIR=../percona-xtrabackup-$XTRA_BACKUPS8_VERSION install \
    && cd ../percona-xtrabackup-$XTRA_BACKUPS8_VERSION \
    && for f in bin lib xtrabackup-test; do \
	cp -rp ./usr/local/xtrabackup/$f $f; \
    done \
    && ls

RUN set -x \
    && echo "# Building Percona Cluster $PXC_BRANCH" \
    && cd /usr/src/percona-xtradb-cluster \
    && ./build-ps/build-binary.sh ./pxc-build \
    && ls -l $PCX_BUILD \
    && mv $PXC_BUILD/Percona-XtraDB-Cluster*minimal.tar.gz /usr/src/percona.tar.gz

FROM docker.io/debian:buster-slim

ENV DEBIAN_FRONTEND=noninteractive \
    PERCONA_VERSION=8.0

# Percona image for OpenShift Origin

LABEL io.k8s.description="Percona offers a MySQL implementation suited for scalability." \
      io.k8s.display-name="Percona ${PERCONA_VERSION}" \
      io.openshift.expose-services="3306:mysql" \
      io.openshift.tags="mysql,percona" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-percona" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="${PERCONA_VERSION}"

COPY config/* /
COPY --from=base /usr/src/percona.tar.gz /usr/src/percona.tar.gz

RUN set -ex \
    && apt-get update \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && echo "# Install Percona dependencies" \
    && apt-get -y install --no-install-recommends wget ca-certificates \
	apt-transport-https procps dumb-init gnupg default-mysql-client \
    && echo "# Install Percona" \
    && tar -C /usr --strip-components=1 -xzf /usr/src/percona.tar.gz \
    && echo "# Configure Percona" \
    && useradd -r -u 1001 -g root mysql \
    && rm -f /etc/mysql/mariadb.conf.d/* \
    && mv /mysqld_safe.cnf /client.cnf /wsrep.cnf \
	/etc/mysql/percona-xtradb-cluster.conf.d/ \
    && mv /my.cnf /percona-xtradb-cluster.cnf /etc/mysql/ \
    && find /etc/mysql/ -name '*.cnf' -print0 \
	| xargs -0 grep -lZE '^(bind-address|log)' \
	| xargs -rt -0 sed -Ei 's/^(bind-address|log)/#&/' \
    && rm -fr /var/log/mysql /var/lib/mysql \
    && echo "# Fixing Permissions" \
    && for d in /var/lib/mysql /var/run /var/run/mysqld /var/log/mysql \
	/etc/mysql /etc/mysql/conf.d; do \
	mkdir -p "$d" \
	&& ( chown -R mysql:root "$d" || echo nevermind ) \
	&& ( chmod -R g=u "$d" || echo nevermind ); \
    done \
    && echo "# Cleanup" \
    && apt-get remove --purge -y wget dirmngr apt-transport-https gnupg \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
	/usr/src/percona.tar.gz \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

USER 1001
ENTRYPOINT [ "/run-percona.sh" ]
CMD [ "mysqld" ]

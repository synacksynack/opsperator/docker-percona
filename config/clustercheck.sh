#!/bin/bash
#
# Authors:
# Raghavendra Prabhu <raghavendra.prabhu@percona.com>
# Olaf van Zandwijk <olaf.vanzandwijk@nedap.com>
# Based on the original script from Unai Rodriguez and Olaf
#   (https://github.com/olafz/percona-clustercheck)

if test "$1" = -h -o "$1" = --help; then
    echo "Usage: $0 <user> <pass> <available_when_donor=0|1> <log_file>"
    echo "    <available_when_readonly=0|1> <defaults_extra_file>"
    exit 0
fi

AVAILABLE_WHEN_DONOR=${1:-1}
AVAILABLE_WHEN_READONLY=${2:-1}
DEFAULTS_EXTRA_FILE="${3:-/etc/mysql/my.cnf}"
ERR_FILE="${4:-/dev/stderr}"
EXTRA_ARGS=
MYSQL_MONITOR_PASSWORD="${MYSQL_MONITOR_PASSWORD:-secret}"
MYSQL_MONITOR_USER="${MYSQL_MONITOR_USER:-monitor}"
TIMEOUT=10

if test "$MYSQL_MONITOR_USER"; then
    EXTRA_ARGS="$EXTRA_ARGS --user=${MYSQL_MONITOR_USER}"
fi
if test "$MYSQL_MONITOR_PASSWORD"; then
    EXTRA_ARGS="$EXTRA_ARGS --password=${MYSQL_MONITOR_PASSWORD}"
fi
if test "$DEFAULTS_EXTRA_FILE"; then
    MYSQL_CMDLINE="mysql --defaults-extra-file=$DEFAULTS_EXTRA_FILE -nNE \
	--connect-timeout=$TIMEOUT ${EXTRA_ARGS}"
else
    MYSQL_CMDLINE="mysql -nNE --connect-timeout=$TIMEOUT ${EXTRA_ARGS}"
fi

ipaddr=$(hostname -i | awk '{print $1}')
hostname=$(hostname)
WSREP_STATUS=($($MYSQL_CMDLINE -e "SHOW GLOBAL STATUS LIKE 'wsrep_%';" \
    2>>$ERR_FILE | grep -A 1 -E 'wsrep_local_state$|wsrep_cluster_status$' \
    | sed -n -e '2p' -e '5p' | tr '\n' ' '))

if [[ ${WSREP_STATUS[1]} == 'Primary' && ( ${WSREP_STATUS[0]} -eq 4 || \
    ( ${WSREP_STATUS[0]} -eq 2 && $AVAILABLE_WHEN_DONOR -eq 1 ) ) ]]; then
    if test "$AVAILABLE_WHEN_READONLY" -eq 0; then
        READ_ONLY=`$MYSQL_CMDLINE -e "SHOW GLOBAL VARIABLES LIKE 'read_only';" \
	    2>>$ERR_FILE | tail -1 2>>$ERR_FILE`
        if test "$READ_ONLY" = ON; then
            # Percona XtraDB Cluster node local state is 'Synced', but it is in
            # read-only mode. The variable AVAILABLE_WHEN_READONLY is set to 0.
            # => return HTTP 503
            # Shell return-code is 1
	    exit 1
        fi
    fi
    # Percona XtraDB Cluster node local state is 'Synced' => return HTTP 200
    # Shell return-code is 0
    exit 0
fi

# Percona XtraDB Cluster node local state is not 'Synced' => return HTTP 503
# Shell return-code is 1
exit 1

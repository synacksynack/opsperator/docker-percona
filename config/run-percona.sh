#!/bin/bash

if test "$DEBUG"; then
    set -x
fi

MYSQL_MONITOR_PASSWORD="${MYSQL_MONITOR_PASSWORD:-secret}"
MYSQL_MONITOR_USER=${MYSQL_MONITOR_USER:-monitor}
MYSQL_REPL_PASSWORD="${MYSQL_REPL_PASSWORD:-secret}"
MYSQL_REPL_USER=${MYSQL_REPL_USER:-xtrabackup}
MYSQL_ROOT_PASSWORD="${MYSQL_ROOT_PASSWORD:-secret}"
MYSQL_PASSWORD="${MYSQL_PASSWORD:-secret}"
if test -z "$POD_NAMESPACE"; then
    POD_NAMESPACE=`hostname -f | cut -d. -f2`
    if test -z "$POD_NAMESPACE"; then
	POD_NAMESPACE=`awk '/search/{print $2}' /etc/resolv.conf | cut -d. -f2`
    fi
fi
if test -z "$PERCONA_HOSTNAME"; then
    PERCONA_HOSTNAME=`hostname | cut -d. -f1`
fi
export POD_NAMESPACE

PERCONA_SERVER_ID=`echo "$PERCONA_HOSTNAME" | sed 's|^.*-\([0-9][0-9]*\)$|\1|'`
if ! test "$PERCONA_SERVER_ID" = "$PERCONA_HOSTNAME"; then
    MY_NAME="$PERCONA_BASENAME-$PERCONA_SERVER_ID.$PERCONA_BASENAME"
    PERCONA_BASENAME=`echo $PERCONA_HOSTNAME | sed "s|-$PERCONA_SERVER_ID$||"`
    SERVER_ID=`expr $PERCONA_SERVER_ID + 1`
else
    MY_NAME="$PERCONA_HOSTNAME"
    PERCONA_BASENAME=
    SERVER_ID=0
fi

list_replicates()
{
    local check

    check=0
    while :
    do
	who=$PERCONA_BASENAME-$check.$PERCONA_BASENAME
	if test "$check" = "$PERCONA_SERVER_ID"; then
	    echo NOTICE: do not configure replication against local
	    check=`expr $check + 1`
	    continue
	elif bash -c "echo dummy >/dev/tcp/$who/4567" >/dev/null 2>&1; then
	    echo NOTICE: $PERCONA_BASENAME-$check is alive
	else
	    echo NOTICE: $who unreachable, assuming remote does not exist
	    break
	fi >&2
	echo $who
	check=`expr $check + 1`
    done
}

should_init=false
should_reset=false
if test -z "$CLUSTER_ADDRESS"; then
    echo "Percona XtraDB Cluster: Initializing Cluster Address"
    CURRENT_MEMBERS=$(list_replicates)
    for member in $CURRENT_MEMBERS
    do
	if ! grep "^$member$" /etc/mysql/conf.d/.members >/dev/null; then
	    echo $member >>/etc/mysql/conf.d/.members
	fi
    done
    MEMBERS=$(cat /etc/mysql/conf.d/.members 2>/dev/null)
    if test -z "$MEMBERS" -a "$PERCONA_SERVER_ID" = 0; then
	CLUSTER_ADDRESS=gcomm://
	should_init=true
    elif test -z "$CURRENT_MEMBERS" -a "$PERCONA_SERVER_ID" = 0; then
	CLUSTER_ADDRESS=gcomm://
	should_reset=true
    elif test -z "$CURRENT_MEMBERS"; then
	cat <<EOF
FATAL: does not know about other members, with ID $PERCONA_SERVER_ID
waiting for first statefulset member to start up
EOF
	exit 1
    else
	if ! grep "^$MY_NAME$" /etc/mysql/conf.d/.members; then
	    echo $MY_NAME >>/etc/mysql/conf.d/.members
	    MEMBERS=$(cat /etc/mysql/conf.d/.members)
	fi
	LIST=$(echo $MEMBERS | tr '\n' ',' | sed -e 's| |,|g' -e 's|,$||')
	CLUSTER_ADDRESS="gcomm://$LIST"
    fi
fi
echo "Percona XtraDB Cluster: Discovered Cluster Address $CLUSTER_ADDRESS"
echo Percona XtraDB Cluster: Initializing Runtime
if test -s /node.cnf -a \! -s /etc/mysql/conf.d/node.cnf; then
    if ls /usr/lib/libgalera_smm.so >/dev/null 2>&1; then
	GALERA_LIB=/usr/lib/libgalera_smm.so
    else
	GALERA_LIB=/usr/lib/galera3/libgalera_smm.so
    fi
    sed -e "s|MYSQL_ROOT_PW|$MYSQL_ROOT_PASSWORD|" \
	-e "s|GALERA_LIB|$GALERA_LIB|" \
	/node.cnf >/etc/mysql/conf.d/node.cnf
fi
if ! test -s /etc/mysql/conf.d/docker.cnf; then
    cat <<EOF >/etc/mysql/conf.d/docker.cnf
[mysqld]
skip-host-cache
skip-name-resolve
EOF
    echo '!include /etc/mysql/conf.d/docker.cnf' >>/etc/mysql/my.cnf
fi
cat <<EOF >/etc/mysql/conf.d/monitor.cnf
[mysqld]
innodb_monitor_enable = module_index,module_icp,module_ddl,module_purge,module_lock,module_transaction,innodb_log_flush_usec,innodb_ibuf_merge_usec,innodb_checkpoint_usec,adaptive_hash_index
EOF
if test -s /node.cnf; then
    if ! grep node.cnf /etc/mysql/my.cnf >/dev/null; then
	echo '!include /etc/mysql/conf.d/node.cnf' >>/etc/mysql/my.cnf
    fi
fi

sed -i -e "s|^wsrep_node_address =.*$|wsrep_node_address = $MY_NAME|" \
    -e "s|^wsrep_cluster_name =.*$|wsrep_cluster_name = $PERCONA_BASENAME|" \
    -e "s|^wsrep_cluster_address =.*$|wsrep_cluster_address = $CLUSTER_ADDRESS|" \
    /etc/mysql/conf.d/node.cnf
echo Percona XtraDB Cluster: Done Configuring Runtime

if $should_init; then
    echo Initializing database
    $@ --initialize-insecure --ignore-db-dir=lost+found
    echo Database initialized
    $@ --skip-networking --ignore-db-dir=lost+found \
	--socket=/var/run/mysqld/mysqld.sock &
    pid="$!"
    cpt=0
    while test "$cpt" -lt 30
    do
	if echo SELECT 1 | mysql --protocol=socket -uroot -hlocalhost \
	    --socket=/var/run/mysqld/mysqld.sock &>/dev/null; then
	    break
	fi
	cpt=`expr $cpt + 1`
	if test "$cpt" = 30; then
	    echo MySQL startup failed
	    exit 1
	fi
	echo MySQL startup in progress...
	sleep 1
    done
    while test "$cpt" -lt 30
    do
	ret=true
	(
	    mysql --protocol=socket -uroot -hlocalhost \
		--socket=/var/run/mysqld/mysqld.sock <<EOSQL
SET @@SESSION.SQL_LOG_BIN=0;
CREATE USER 'root'@'%' IDENTIFIED BY '$MYSQL_ROOT_PASSWORD';
GRANT ALL ON *.* TO 'root'@'%' WITH GRANT OPTION ;
ALTER USER 'root'@'localhost' IDENTIFIED BY '$MYSQL_ROOT_PASSWORD';
CREATE USER '$MYSQL_REPL_USER'@'localhost' IDENTIFIED BY '$MYSQL_REPL_PASSWORD';
GRANT RELOAD,PROCESS,LOCK TABLES,REPLICATION CLIENT ON *.* TO '$MYSQL_REPL_USER'@'localhost';
CREATE USER '$MYSQL_MONITOR_USER'@'127.0.0.1' IDENTIFIED BY '$MYSQL_MONITOR_PASSWORD';
CREATE USER '$MYSQL_MONITOR_USER'@'localhost' IDENTIFIED BY '$MYSQL_MONITOR_PASSWORD';
GRANT PROCESS ON *.* TO '$MYSQL_MONITOR_USER'@'127.0.0.1';
GRANT PROCESS ON *.* TO '$MYSQL_MONITOR_USER'@'localhost';
GRANT SELECT ON performance_schema.* TO '$MYSQL_MONITOR_USER'@'127.0.0.1';
GRANT SELECT ON performance_schema.* TO '$MYSQL_MONITOR_USER'@'localhost';
GRANT REPLICATION CLIENT ON *.* TO '$MYSQL_MONITOR_USER'@'127.0.0.1';
GRANT REPLICATION CLIENT ON *.* TO '$MYSQL_MONITOR_USER'@'localhost';
GRANT REPLICATION SLAVE ON *.* TO '$MYSQL_MONITOR_USER'@'127.0.0.1';
GRANT REPLICATION SLAVE ON *.* TO '$MYSQL_MONITOR_USER'@'localhost';
DROP DATABASE IF EXISTS test;
FLUSH PRIVILEGES;
EOSQL
	) || ret=false
	if $ret; then
	    break
	fi
	cpt=`expr $cpt + 1`
	if test "$cpt" = 30; then
	    echo MySQL accounts init failed
	    exit 1
	fi
	echo MySQL accounts init in progress...
	sleep 1
    done
    (
	if test "$MYSQL_DATABASE"; then
	    cat <<EOSQL
CREATE DATABASE IF NOT EXISTS \`$MYSQL_DATABASE\`;
EOSQL
	fi
	if test "$MYSQL_USER" -a "$MYSQL_PASSWORD"; then
	    cat <<EOSQL
CREATE USER '$MYSQL_USER'@'%' IDENTIFIED BY '$MYSQL_PASSWORD';
EOSQL
	    if test "$MYSQL_DATABASE"; then
		cat <<EOSQL
GRANT ALL ON \`$MYSQL_DATABASE\`.* TO '$MYSQL_USER'@'%';
EOSQL
	    fi
	    echo 'FLUSH PRIVILEGES;'
	fi
    ) | mysql --protocol=socket -uroot -hlocalhost \
	--socket=/var/run/mysqld/mysqld.sock "-p$MYSQL_ROOT_PASSWORD"
    if test -d /dbinit; then
	for f in $(ls /dbinit/* 2>/dev/null)
	do
	    case "$f" in
		*.sh) echo "$0: running $f"; . "$f" ;;
		*.sql)
		    echo "$0: importing $f"
		    mysql --protocol=socket -uroot -hlocalhost \
			--socket=/var/run/mysqld/mysqld.sock \
			"-p$MYSQL_ROOT_PASSWORD" <"$f" ;;
		*.sql.gz)
		    echo "$0: importing $f"
		    gunzip -c "$f" | mysql --protocol=socket -uroot \
			--socket=/var/run/mysqld/mysqld.sock \
			-hlocalhost "-p$MYSQL_ROOT_PASSWORD" ;;
		*) echo "$0: ignoring $f" ;;
	    esac
	    echo
	done
    fi
    if ! kill -s TERM "$pid" || ! wait "$pid"; then
	echo MySQL init process failed
	exit 1
    fi
    echo
    echo MySQL init process done. Ready for start up.
    echo
elif $should_reset; then
    sed -i 's|safe_to_bootstrap:.*|safe_to_bootstrap: 1|' \
	/var/lib/mysql/grastate.dat
elif test $PERCONA_SERVER_ID = 0; then
    sed -i 's|safe_to_bootstrap:.*|safe_to_bootstrap: 0|' \
	/var/lib/mysql/grastate.dat
fi

unset MYSQL_ROOT_PASSWORD MYSQL_MONITOR_PASSWORD MYSQL_MONITOR_USER \
    MYSQL_PASSWORD MYSQL_USER MYSQL_DATABASE PERCONA_HOSTNAME pid \
    PERCONA_BASENAME PERCONA_SERVER_ID NAMESPACE POD_NAMESPACE \
    CLUSTER_ADDRESS

exec $@ --ignore-db-dir=lost+found \
    --wsrep_sst_auth="$MYSQL_REPL_USER:$MYSQL_REPL_PASSWORD"

# k8s Percona

Forked from https://github.com/docker-library/percona

Cluster-capable, OpenShift friendly, Docker Percona image.

Build with:

```
$ make build
```

Build in OpenShift:

```
$ make ocbuild
```

Start cluster on OpenShift:

```
$ make ocprod
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name            |    Description                | Default                               |
| :-------------------------- | ----------------------------- | ------------------------------------- |
|  `MYSQL_DATABASE`           | Percona applicative DB        | undef                                 |
|  `MYSQL_MONITOR_PASSWORD`   | Percona Monitoring Password   | `secret`                              |
|  `MYSQL_MONITOR_USER`       | Percona Monitoring Username   | `monitor`                             |
|  `MYSQL_PASSWORD`           | Percona applicative Password  | `secret`                              |
|  `MYSQL_REPL_PASSWORD`      | Percona Replication Password  | `secret`                              |
|  `MYSQL_REPL_USER`          | Percona Replication Username  | `xtrabackup`                          |
|  `MYSQL_ROOT_PASSWORD`      | Percona Root Password         | `secret`                              |
|  `MYSQL_USER`               | Percona applicative Username  | undef                                 |
|  `PERCONA_HOSTNAME`         | Percona Member Hostame        | deduced from hostname                 |
|  `POD_NAMESPACE`            | Percona K8s Namespace         | deduced from hostname or resolv.conf  |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point   | Description                  |
| :-------------------- | ---------------------------- |
|  `/var/lib/mysql`     | Percona Database             |
|  `/etc/mysql/conf.d`  | Percona Local Configuration  |

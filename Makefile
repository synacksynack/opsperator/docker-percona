SKIP_SQUASH?=1
FRONTNAME=opsperator
IMAGE = opsperator/percona
-include Makefile.cust

.PHONY: build
build:

	SKIP_SQUASH=$(SKIP_SQUASH) hack/build.sh

.PHONY: demo
demo:
	@@docker rm -f testmysql || true
	@@docker run --name testmysql \
	    --add-host=percona-0:127.0.0.1 \
	    --hostname=percona-0 \
	    -e MYSQL_DATABASE=msdb \
	    -e MYSQL_MONITOR_PASSWORD=msmonitpw \
	    -e MYSQL_MONITOR_USER=msmonituser \
	    -e MYSQL_PASSWORD=mspassword \
	    -e MYSQL_REPL_PASSWORD=msreplpw \
	    -e MYSQL_REPL_USER=msrepluser \
	    -e MYSQL_USER=msuser \
	    -e PERCONA_HOSTNAME=percona-0 \
	    -e POD_NAMESPACE=default \
	    -p 3306:3306 \
	    -d $(IMAGE)

.PHONY: test
test:
	docker exec testmysql /bin/bash -c '/clustercheck.sh && echo OK' || echo wamp wamp;
	docker exec testmysql /bin/bash -c '/clustercheck.sh && echo OK' | grep OK || echo wamp wamp;

.PHONY: kubebuild
kubebuild: kubecheck
	@@for f in image git task pipeline pipelinerun; \
	    do \
		kubectl apply -f deploy/kubernetes/tekton-$$f.yaml; \
	    done

.PHONY: kubecheck
kubecheck:
	@@kubectl version >/dev/null 2>&1 || exit 42

.PHONY: ocbuild
ocbuild: occheck
	@@oc process -f deploy/openshift/imagestream.yaml | oc apply -f-
	@@BRANCH=`git rev-parse --abbrev-ref HEAD`; \
	if test "$$GIT_DEPLOYMENT_TOKEN"; then \
	    oc process -f deploy/openshift/build-with-secret.yaml \
		-p "GIT_DEPLOYMENT_TOKEN=$$GIT_DEPLOYMENT_TOKEN" \
		-p "PERCONA_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	else \
	    oc process -f deploy/openshift/build.yaml \
		-p "PERCONA_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	fi

.PHONY: occheck
occheck:
	@@oc whoami >/dev/null 2>&1 || exit 42

.PHONY: occlean
occlean:
	@@oc process -f deploy/openshift/secret.yaml -p FRONTNAME=$(FRONTNAME) \
	    | oc delete -f- || true
	@@oc process -f deploy/openshift/run-ha.yaml -p FRONTNAME=$(FRONTNAME) \
	    | oc delete -f- || true

.PHONY: ocprod
ocprod: ocbuild
	@@if ! oc describe secret percona-$(FRONTNAME) >/dev/null 2>&1; then \
	    oc process -f deploy/openshift/secret.yaml \
		-p FRONTNAME=$(FRONTNAME) | oc apply -f-; \
	fi
	@@oc process -f deploy/openshift/run-ha.yaml -p FRONTNAME=$(FRONTNAME) \
	    | oc apply -f-

.PHONY: ocpurge
ocpurge:
	@@oc process -f deploy/openshift/build.yaml -p FRONTNAME=$(FRONTNAME) \
	    | oc delete -f- || true
	@@oc process -f deploy/openshift/imagestream.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc delete -f- || true

FROM docker.io/debian:buster-slim

ENV DEBIAN_FRONTEND=noninteractive \
    PERCONA_BASE=percona-xtradb-cluster-server \
    PERCONA_MAJOR=5.7

# Percona image for OpenShift Origin

LABEL io.k8s.description="Percona offers a MySQL implementation suited for scalability." \
      io.k8s.display-name="Percona ${PERCONA_MAJOR}" \
      io.openshift.expose-services="3306:mysql" \
      io.openshift.tags="mysql,percona,mysql5,percona5" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-percona" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="${PERCONA_MAJOR}.36"

COPY config/* /

RUN set -ex \
    && apt-get update \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && echo "# Install Percona dependencies" \
    && apt-get -y install --no-install-recommends wget ca-certificates \
	apt-transport-https procps dumb-init gnupg \
    && echo "# Install Percona" \
    && echo 'deb https://repo.percona.com/apt buster main' \
	>/etc/apt/sources.list.d/percona.list \
    && for key in $PERCONA_BASE/root_password \
		$PERCONA_BASE/root_password_again \
		"$PERCONA_BASE-$PERCONA_MAJOR/root-pass" \
		"$PERCONA_BASE-$PERCONA_MAJOR/re-root-pass"; \
	do \
	    echo "$PERCONA_BASE-$PERCONA_MAJOR" "$key" password 'unused'; \
	done | debconf-set-selections \
    && useradd -r -u 1001 -g root mysql \
    && wget -O- \
	"https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x9334A25F8507EFA5" \
	| apt-key add - \
    && apt-get update \
    && apt-get install -y $PERCONA_BASE-$PERCONA_MAJOR \
    && echo "# Configure Percona" \
    && mv /my.cnf /percona-xtradb-cluster.cnf /etc/mysql/ \
    && sed -ri 's/^user\s/#&/' /etc/mysql/my.cnf \
    && find /etc/mysql/ -name '*.cnf' -print0 \
	| xargs -0 grep -lZE '^(bind-address|log)' \
	| xargs -rt -0 sed -Ei 's/^(bind-address|log)/#&/' \
    && rm -fr /var/log/mysql /var/lib/mysql \
    && echo "# Fixing Permissions" \
    && for d in /var/lib/mysql /var/run /var/run/mysqld /var/log/mysql \
	/etc/mysql /etc/mysql/conf.d; do \
	mkdir -p "$d" \
	&& ( chown -R mysql:root "$d" || echo nevermind ) \
	&& ( chmod -R g=u "$d" || echo nevermind ); \
    done \
    && echo "# Cleanup" \
    && apt-get remove --purge -y wget dirmngr apt-transport-https gnupg \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
	/mysqld_safe.cnf /client.cnf /swrep.cnf \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

USER 1001
ENTRYPOINT [ "/run-percona.sh" ]
CMD [ "mysqld" ]
